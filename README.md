# QR code component solution

### Built with

- HTML5
- CSS3

### Continued development . . .

- `Todos: Make it dynamic`

### Useful resources

- Figma

### Author

- Website - [Brian Opedal](https://brian.engineer)
